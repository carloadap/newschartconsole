﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataManagement;
using DataLayer.Model;
using DataLayer;

namespace NewsChartConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            List<NewsNadexCurrency> news = NewsManager.GetNews();
            NewsManager manager = new NewsManager();
            manager.uploadNewsChart(news);
        }
    }
}
